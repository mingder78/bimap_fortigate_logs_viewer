# logstash.conf                                                        X
```
 input {
   snmp {
    # diskusage(KB), cpuusage(%), memusage(%)
     get => ["1.3.6.1.4.1.12356.101.4.1.6.0", "1.3.6.1.4.1.12356.101.4.1.  3.0", "1.3.6.1.4.1.12356.101.4.1.4.0"]
    # host ip is fortigate VM ip
     hosts => [{host => "udp:10.99.1.200/161" community => "public"}]
     tags => ["snmp"]
   }
```
上方 10.99.1.200 是 fortigate IP, 必須改

# notes
## 2020/3/2 會議紀錄

安裝設定上仍有問題 - 這週用room進行討論改善
Alert mail 直接送出而不用透過第三方轉送 - mail server 有很多種類，設定要和工程師/MIS合作進行，這週用exchange測試smtp，請群環提供fortinet 發送email的設定頁面，bimap提供設定操作手冊
另外要定5個 alert rules, 也請群環提供
提供 release note -
