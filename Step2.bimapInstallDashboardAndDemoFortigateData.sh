cd odfe-docker

# import demo forigate index
curl -k -u admin:admin -XPOST "https://localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' --data-binary @logstash-fortigate-demo-index-data.json

# import dashboard
curl -k -u admin:admin -X POST "localhost:5601/api/saved_objects/_import?overwrite=true" -H "kbn-xsrf: true" --form file=@Dashboard3.ndjson
cd ..
