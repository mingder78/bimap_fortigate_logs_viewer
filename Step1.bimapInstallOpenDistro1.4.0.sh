#!/bin/sh
# v0.1.0
# for both centos 7.4~7.7 or ubuntu 18.04

for_centos7()
{
# install pip and python 
sudo yum install -y epel-release
sudo yum install -y python-pip
sudo pip install --upgrade pip
sudo pip install elasticsearch

#---------- remove docker ce
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine

#-------- install docker ce

sudo yum install -y yum-utils \
      device-mapper-persistent-data \
      lvm2

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum-config-manager --disable docker-ce-nightly

sudo yum install -y docker-ce docker-ce-cli containerd.io
#--------- start docker server

sudo service docker start
sudo systemctl enable docker.service

#--------- add firewall udp port 5140

sudo yum install -y nc nmap tcpdump
sudo yum install -y firewalld
sudo service firewalld start
sudo firewall-cmd --zone=public --add-port=5140/udp --permanent
sudo firewall-cmd --zone=public --add-port=5162/udp --permanent

sudo firewall-cmd --reload
sudo firewall-cmd --list-all

}

for_ubuntu() {
echo "run for ubuntu"
sudo apt-get remove docker docker-engine docker.io
sudo apt install -y docker.io
sudo systemctl start docker
sudo systemctl enable docker
}

run_docker_compose() {
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose


#------------ report versions

cat /etc/*release*
sudo docker version
docker-compose --version

#--------- must have patch for docker to run ELK
sudo sysctl -w vm.max_map_count=262144
sudo echo "vm.max_map_count=262144" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

#--------- install elk
#mkdir odfe-docker
cd odfe-docker
#wget https://gist.githubusercontent.com/mingderwang/cec223c11c7195c640924f86e9c78f8b/raw/dba6b7bf95c87972df94364bc8757011de7f7b81/logstash.conf
#wget https://gist.githubusercontent.com/mingderwang/1770cdcf9ad7d751055cf45f32313876/raw/7194dc3e518cb2c92c01da41a255a4ef2ad7de8d/docker-compose.yml
sudo /usr/local/bin/docker-compose up -d
}

#------ start here

cmd="sudo yum update"
$cmd
## get status ##
status=$?
if [ $status -eq "0" ]
then
  for_centos7
else
  for_ubuntu
fi
run_docker_compose
exit
